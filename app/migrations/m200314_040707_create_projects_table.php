<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%projects}}`.
 */
class m200314_040707_create_projects_table extends Migration
{
    protected $tableName = 'projects';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%projects}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'price' => $this->integer()->notNull(),
            'start_date' => $this->date(),
            'completion_date' => $this->date(),
        ]);

        $usersTable = \app\models\tables\Users::tableName();

        $this->addForeignKey('fk_responsible_id', $this->tableName,
            'user_id', $usersTable, 'id', 'cascade', 'no action');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_responsible_id', $this->tableName);

        $this->dropTable('{{%projects}}');
    }
}