<?php

use yii\db\Migration;

/**
 * Class m200314_051825_add_default_user
 */
class m200314_051825_add_default_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('users', [
            'full_name' => 'admin',
            'login' => 'admin',
            'password' => 'admin',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('users');
    }
}
