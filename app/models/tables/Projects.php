<?php

namespace app\models\tables;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $name
 * @property int $price
 * @property string|null $start_date
 * @property string|null $completion_date
 *
 * @property Users $user
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'price'], 'integer'],
            [['name', 'price'], 'required'],
            [['start_date', 'completion_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'price' => 'Price',
            'start_date' => 'Start Date',
            'completion_date' => 'Completion Date',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
